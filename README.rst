Rust Traits
============

Rust traits look a lot like typeclasses in Haskell.

This is an attempt at a Rust port of `UPenn CS194 Homework 5`_, which explores
Haskell typeclasses.

.. _UPenn CS194 Homework 5: https://www.seas.upenn.edu/~cis194/spring13/hw/05-type-classes.pdf
